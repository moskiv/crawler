package Avalanche;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AvalancheTT {

    private final Set<URL> links;
    private final Set<String> emails;

    // Declaring constructor for links, emails
    // and main page processor method
    private AvalancheTT(final URL startURL){
        this.links = new HashSet<>();
        this.emails = new HashSet<>();
        pageProcesser(initURLS(startURL));
    }

    //Return start url as singleton
    private Set<URL> initURLS(final URL startURL){
        return Collections.singleton(startURL);
    }

    private void pageProcesser( final Set<URL> urls) {
        // Remove URLs that has been visited
        urls.removeAll(this.links);

        if(!urls.isEmpty()){
            final Set<URL> newURLS = new HashSet<>();
            try{
                this.links.addAll(urls);
                for(final URL url : urls){
                    System.out.println("Processing: " + url);
                    final Document document = Jsoup.connect(url.toString()).get();

                    // looking for email pattern on a page
                    Pattern p = Pattern.compile("[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}", Pattern.CASE_INSENSITIVE);
                    Matcher matcher = p.matcher(document.text());
                    while (matcher.find()) {
                        this.emails.add(matcher.group());
                        for(String email : emails){
                            System.out.println("Found email : " + email + " on page " + url);
                        }
                    }
                    //Finding internal urls and adding them to newURL list
                    final Elements linksOnPage = document.select("a[href]");
                    for(final Element element : linksOnPage){
                        final String urlText = element.attr("abs:href");
                        final URL discoveredURL = new URL(urlText);
                        newURLS.add(discoveredURL);
                    }
                }
            } catch (final Exception | Error ignored ){
            }
            //Recursively calling method with new found urls
            pageProcesser(newURLS);
        }
    }
        // Storing Emails into a file
        private void saveEmail() throws IOException {
        FileWriter fileWriter = new FileWriter("EmailOutput.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
            for(String email : this.emails) {
                printWriter.write(email + "\n");
            }
            printWriter.close();

        File output = new File("EmailOutput");
        System.out.println( "All emails are stored in: " + output.getAbsolutePath());
  }

    // Main method where initial starting page is declared
    public static void main(String[] args) throws IOException{

        System.out.println("Enter a webpage to initiate email search: ");
        Scanner scanner = new Scanner(System.in);
        String html = scanner.nextLine();
        System.out.println("Looking in :" + html);

        // Comment Scanner and uncomment String html to use it for as default address
        //String html = "https://www.avalanchelabs.ee";

        final AvalancheTT task = new AvalancheTT(new URL( html));
        task.saveEmail();
    }
}
